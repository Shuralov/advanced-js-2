const cal = {
    SIZE_SMALL : 20,
    SIZE_LARGE : 40,
    STUFFING_CHEESE: 20,
    STUFFING_SALAD: 5,
    STUFFING_POTATO: 10,
    TOPPING_MAYO: 5,
    TOPPING_SPICE: 0
}

const price = {
    SIZE_SMALL : 50,
    SIZE_LARGE : 100,
    STUFFING_CHEESE: 10,
    STUFFING_SALAD: 20,
    STUFFING_POTATO: 15,
    TOPPING_MAYO: 20,
    TOPPING_SPICE: 15
}

class Hamburger {

    constructor (size, stuffing) {
        this.SIZE = size;
        this.STUFFING = stuffing;
        this.toppings = [];
        if (size === 'STUFFING_CHEESE' && 'STUFFING_SALAD' && 'STUFFING_POTATO') {
           throw new HamburgerException(`First should go the size not a ${size}`);
        }

            if (stuffing !== 'STUFFING_CHEESE' && 'STUFFING_SALAD' && 'STUFFING_POTATO') {
                throw new HamburgerException(`Please add stuffing`);
    }


//Shallow cloning
    let argsCopy = [];
    for (let b = 0; b < arguments.length; b++) {
        argsCopy[b] = arguments[b];
    }

//Duplication check in case of adjacent copies
    for (let a=0; a<argsCopy.length; a++) {
        if (argsCopy[a]===argsCopy[a+1]) {
            throw new HamburgerException (`HamburgerException: duplicated topping ${argsCopy[a]}`);
        }
    }
    }

        addTopping (topping) {
            this.toppings.push(topping);
            return this.toppings;
    }



    calculateCalories () {
        let calSet = Object.keys(cal);
        let objSet = Object.values(this).flat();
        let counter = 0;

        for (let i=0; i<calSet.length; i++ ) {
            for (let j=0; j<objSet.length; j++ ) {
                if (calSet[i] === objSet[j]) {
                    counter = counter + cal[calSet[i]];
                }
            }
        }
        return counter;
    }


        calculatePrice () {
        let priceSet = Object.keys(price);
        let objSet = Object.values(this).flat();
        let accum = 0;

        for (let i=0; i<priceSet.length; i++ ) {
            for (let j=0; j<objSet.length; j++ ) {
                if (priceSet[i] === objSet[j]) {
                    accum = accum + price[priceSet[i]];
                }
            }
        }
        return accum;
    }

        removeTopping (topping) {
        if (this.toppings.includes(topping)) {
            this.toppings.pop();
        }
    }

        getToppings () {
        return this.toppings;
    }

        getSize () {
            return this.SIZE;
        }

        getStuffing () {
        return this.STUFFING;
    }
}

function HamburgerException(message) {
    this.message = message;
}

Hamburger.SIZE_SMALL = 'SIZE_SMALL';
Hamburger.SIZE_LARGE = 'SIZE_LARGE';
Hamburger.STUFFING_CHEESE = 'STUFFING_CHEESE';
Hamburger.STUFFING_SALAD = 'STUFFING_SALAD';
Hamburger.STUFFING_POTATO = 'STUFFING_POTATO';
Hamburger.TOPPING_MAYO = 'TOPPING_MAYO';
Hamburger.TOPPING_SPICE = 'TOPPING_SPICE';

// маленький гамбургер с начинкой из сыра
let hamburger = new Hamburger('SIZE_SMALL','STUFFING_CHEESE');
//добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
//сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1


